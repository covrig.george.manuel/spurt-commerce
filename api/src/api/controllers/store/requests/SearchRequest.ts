
import 'reflect-metadata';

export class SearchByImageRequest {
    public image: string;
    public imageName: string;
}
