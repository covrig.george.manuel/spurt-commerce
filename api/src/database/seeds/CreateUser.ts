import { Connection } from 'typeorm';
import { Factory, Seed } from 'typeorm-seeding';
import { User } from '../../api/models/User';
export class CreateUser implements Seed {

    public async seed(factory: Factory, connection: Connection): Promise<User> {
        console.log("suntem in create user")
        const em = connection.createEntityManager();
        const user = new User();
        user.userId = 1;
        user.username = 'covrig.george.manuel@gmail.com';
        user.password = await User.hashPassword('cart');
        user.email = 'covrig.george.manuel@gmail.com';
        user.userGroupId = 1;
        return await em.save(user);
    }
}
