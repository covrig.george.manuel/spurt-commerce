export const environment = {
  production: true,
  baseUrl: '', // <Your API base url>
  imageUrl: '', // <Your API url for image resize>
  productUrl: 'http://localhost:9000/api/' // <Your store base url>
};
