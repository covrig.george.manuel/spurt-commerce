import { Component, OnInit } from '@angular/core';
import { ProductControlService } from 'src/core/product-control/product-control.service';

@Component({
  selector: 'app-drag-and-drop',
  templateUrl: './drag-and-drop.component.html',
  styleUrls: ['./drag-and-drop.component.scss']
})
export class DragAndDropComponent implements OnInit {
  files: any = [];
  constructor(private productService: ProductControlService) {

  }
  uploadFile(event) {
    let img = this.convertBase64(event.target)

  }
  uploadChange($event): void {
    this.convertBase64($event.target);
  }

  // convert image file into Base64 format
  convertBase64(inputValue: any): string {
    debugger
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    var img;
    myReader.onloadend = (e) => {
      var params: any = { image: "" };
      params.image = myReader.result;
      img = myReader.result;
      this.productService.searchByImage(params).subscribe(
        response => {
          this.productService.value.emit(response.data)
          debugger
        }, err => {
          console.log(err)
        }
      )
    };
    myReader.readAsDataURL(file);
    return img
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }


  ngOnInit() {
  }

}
